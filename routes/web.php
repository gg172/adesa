<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('home', function () {
    return view('template.index')->name('home');
});

Route::get('/login', function () {
    return view('login.login');
});

Route::get('/signin', function () {
    return view('login.signin');
});

Route::get('/usulan', function () {
    return view('pengajuan.usulan');
});

Route::get('/profil', function () {
    return view('profil.profil');
});

Route::get('/pengumuman', function () {
    return view('pengumuman.pengumuman');
});

Route::get('/', 'App\Http\Controllers\HomeController@index')->name('home');

Route::get('/pengumuman', 'App\Http\Controllers\PengumumanController@index')->name('pengumuman');