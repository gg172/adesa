@extends('template.index')
@section('konten')
<form class="p-5 card shadow mt-5 mx-5 text-center">
    <h3 class="text-center fw-bold">Login</h3>
    <p class="text-center"> Please login to your account</p>
    <!-- Email input -->
    <div class="form-outline mb-4">
        <input type="email" id="form2Example1" class="form-control border border-black"  placeholder="Email" require/>
        
    </div>

    <!-- Password input -->
    <div class="form-outline mb-5">
        <input type="password" id="form2Example2" class="form-control border border-black"  placeholder="Password" require/>
        
    </div>

    <!-- 2 column grid layout for inline styling -->
    <div class="row mb-4">
        <div class="col d-flex justify-content-center">
            <!-- Checkbox -->
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="form2Example31" checked />
                <label class="form-check-label" for="form2Example31"> Remember me </label>
            </div>
        </div>

        <div class="col">
            <!-- Simple link -->
            <a href="#!" class="">Forgot password?</a>
        </div>
    </div>

    <!-- Submit button -->
    <button type="button" class="btn btn-blues fw-bold text-white btn-block mb-4">Login</button>

    <!-- Register buttons -->
    <div class="text-center">
        <p>Don't have an account? <a href="signin" class="text-decoration-none">Sign in</a></p>
       
    </div>
</form>
@endsection