<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ADESA</title>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Baloo Bhai' rel='stylesheet'>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
</head>

<body >
    <div class="global-container">
        <nav class="navbar navbar-expand-lg bg-body-blues position-relative">
            <div class="container-fluid">
                <a class="navbar-brand ms-4 nav-adesa text-white" href="#">ADESA</a>
                <button class="navbar-toggler " type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse m " id="navbarNavAltMarkup">
                    <div class="navbar-nav position-absolute top-0 end-0   ">
                        <a class="nav-link active text-white  link-home" aria-current="page" href="{{ route('home') }}">Home</a>
                        <a class="nav-link text-white " href="{{ route('pengumuman') }}">Pengumuman</a>
                        <a class="nav-link text-white" href="#">Pengajuan</a>
                        <h1 class="bi bi-person-circle text-white"></h1>
                        <!-- <img src="{{ asset('assets/img/profil.png') }}" width="40" height="40" class="me-5" alt="" srcset=""> -->
                    </div>
                </div>
                
            </div>
        </nav>
        
        <div class="container">
    @yield('konten')
  </div>
    </div>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
</body>

</html>