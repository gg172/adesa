@extends('template.index')
@section('konten')
<div class="container mt-5">
    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a class="text-decoration-none fw-medium" href="#">Pengumuman</a></li>
            <li class="breadcrumb-item active" aria-current="page">Pengumuman pendaftaran penyediaan alat kebersihan Masjid</li>
        </ol>
    </nav>
    <button class="btn-green  px-3 py-1 mt-3 me-3">Diterima</button>
    <button class="btn-blues text-white   px-3 py-1 mt-3"> <i class="bi bi-download me-3"></i> Unduh Dokumen Persiapan Pengadaan</button>
  

</div>
@endsection