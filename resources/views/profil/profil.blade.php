@extends('template.index')
@section('konten')
<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-8">
            <div class="card shadow">
                <!-- <img src="{{ asset('assets/img/profil.png') }}" width="75px" class="mt-3 mb-5" alt="" srcset=""> -->
                <h1 class="bi bi-person-circle text-center mt-5 mb-5"></h1>
                <ul class="fw-medium mb-5">
                    <li class="list-group-item">Nama : Sunarto</li>
                    <li class="list-group-item">Alamat : Sendowo, Mlati</li>
                    <li class="list-group-item">Email : Sunarto@gmail.com</li>
                    <li class="list-group-item">No Telp : 085xxxxxx</li>
                </ul>
            </div>
        </div>
        <div class="col-3">
            <div class="card bg-body-blues mb-5">
                <div class="card p-2 mb-3 pb-3">
                    <p class="fw-medium">Total Usulan</p>
                    <h4 class="fw-bold">100</h4>
                </div>
            </div>
            <div class="card bg-warning">
                <div class="card p-2 mb-3 pb-3">
                    <p class="fw-medium">Menunggu Respons</p>
                    <h4 class="fw-bold">100</h4>
                </div>
            </div>
        </div>

        <div class="card col-md-11  px-5 pb-3 mt-5">
            <div class="d-flex  mb-5 mt-4">
            <p class="fw-medium me-auto">Daftar Penajuan Usulan</p>
            <a href="" class="btn btn-blues text-white fw-medium p-2">Upload RKP</a>
            </div>
           
            <div class="table-responsive ">
                <table class=" table">
                    <tr class="table-primary">
                        <th>NO.</th>
                        <th>Tanggal</th>
                        <th>Nama Pengusul</th>
                        <th>Kegiatan</th>
                        <th>Biaya</th>
                        <th>Status</th>
                        <th>Detail</th>
                    </tr> 
                    <tr>
                        <td>Jill</td>
                        <td>Smith</td>
                        <td>50</td>
                        <td>50</td>
                        <td>50</td>
                        <td>50</td>
                        <td>50</td>
                        
                    </tr>
                    <tr>
                        <td>Eve</td>
                        <td>Jackson</td>
                        <td>94</td>
                        <td>94</td>
                        <td>94</td>
                        <td>94</td>
                        <td>94</td>
                        
                    </tr>
                    <tr>
                        <td>Adam</td>
                        <td>Johnson</td>
                        <td>67</td>
                        <td>67</td>
                        <td>67</td>
                        <td>67</td>
                        <td>67</td>
                        
                    </tr>
                </table>
            <p class="pt-2">Tampilkan Halaman Per</p>
            </div>
        </div>
    </div>
</div>
@endsection